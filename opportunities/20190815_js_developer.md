Company: Medicspot
Location: London, Hastings and/or remote
Contact: Oliver Brooks - oli[at]medicspot.co.uk

while (true) { medicspot++ };
-----------------------------

Medicspot technology turns any private room with an internet connection into a mini GP surgery. We have developed a unique clinical station which allows a remote doctor to have a video consultation with a patient and perform a medical examination listening to heart and lungs, examining ears, throat and skin, taking blood pressure, oxygen saturation and heart rate.

We create healthcare clinics in pharmacies throughout the country. We've grown to over 250 clinics in a year. Our patients love us with numbers growing 15% month-on-month and a net promoter score of over 70. The industry also recognises our excellence and we were awarded the highest marks of any Health tech company in our CQC (industry body) report.

It works like this; Patient's book or walk into one of our clinics and sit at a clinical station. A doctor joins via video chat and can use the connected medical devices allowing a doctor to provide a thorough, safe, clinical examination. This means we treat more conditions than any other service. Any required medication is immediately available at the local pharmacy (most Medicspot clinics are within pharmacies).

medicspot = [...team, you];
-----------------------------

We're a small, fast moving and growing team. We're friendly, supportive and ambitious and have a big vision to improve lives through healthcare innovation. We're have a strong culture of trust and respect and are looking for an engineer who has the aptitude to learn and apply themselves to build amazing products. We love delivering products which our patients and customers love, solving problems at their core and are continually looking for better ways to work.

import x from 'you';
-----------------------------

Medicspot software is built with developer friendliness and productivity in mind. So expect docker-compose up to get everything up and running. We like products to be beautiful inside and out. We create clean, consistent react and node architectures for webapps and API services.

We can support your journey learning React, node, SOA, testing, docker, HTML5, UX and accessibility and are keen to learn from your experiences too.

The Medicspot system is made up of many discrete products which work together to power the service our patients love. We're looking for a JS developer to assist in the development of a variety of services making them the very best they can be. As a high growth company and there are many career growth opportunities as our team and product evolves.

This role may be for you if you're looking for:

* An environment of learning and productivity
* 1-1 mentorship
* Working with and helping the wider team achieve their goals
* Well defined and simple module/app concerns and interfaces
* JS (node 10+, ES6+)
* React (or at least the declarative pattern it embodies)
* Flat service oriented architectures (REST, JWT)
* Postgres
* Sensible testing
* VCS, CI, CD

@returns {Promise} fun, challenge, cash and perks
-----------------------------

As one of a small team of engineers you are a super important hire. I hope the team, vision and product will be enough to convince you of the opportunity but please tell us what makes you tick and we'll do our best to accommodate.

* For the starter package we're thinking:
* Flexible working hours
* Work from home or the office
* 25 days + bank holidays
* Equipment & training budget

I've been in startups where everyone's been so absorbed by the challenge of the work that good people who have learned new valuable skills have been neglected. We'll regularly review the package to ensure great team players are well rewarded.


Meet the founders
-----------------------------

Zubair - CEO, NHS GP & entrepreneur, London business school.  Zubair owned and operated GP surgeries in Scotland before moving to London to get his MBA. He started Medicspot out of London Business School with the mission of improving the quality of and access to healthcare. He built a prototype and won the LBS best business award. He personally invested in Medicspot and proved the model which is rapidly delivering on his mission in healthcare.

Oli - CTO, 4x startup founder, Cambridge University.  Oli started his first companies while studying engineering, he is now non-exec at successful green tech and publishing technology companies he founded and has recently sold a marketing software platform. He's been a speaker at over 20 conferences including W3C New York. When not building products and software he's out kitesurfing, biking and trying to keep up with his two young children on the south coast. 

How to apply
-----------------------------

So please contact us if you think you'll be a good fit. We start with a call so we can answer any initial questions you might have. We then plan an afternoon, go for a coffee/lunch and then a couple of hours working on an interesting challenge together.

Whatever path you choose we wish you all the best and very much hope to meet you soon!

Oli