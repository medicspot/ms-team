Progressive JavaScript Engineer
========================

Company: [Medicspot](https://www.medicspot.co.uk)
Location: [London](https://goo.gl/maps/pHM7ceA85ZG2)
Contact: Oliver Brooks - oli[at]medicspot.co.uk

while (true) { medicspot++ };
-----------------------------

Medicspot technology turns any private room with an internet connection into a mini GP surgery. We have developed a unique clinical station which allows a remote doctor to have a video consultation with a patient and perform a medical examination listening to heart and lungs, examining ears, throat and skin, taking blood pressure, oxygen saturation and heart rate. 

We create healthcare clinics in pharmacies throughout the country. We've grown to over 250 clinics in a year. Our patients love us with numbers growing 15% month-on-month and a net promoter score of over 70. The industry also recognises our excellence and we were awarded the highest marks of any Health tech company in our CQC (industry body) report.

It works like this; Patient's book or walk into one of our clinics and sit at a clinical station. A doctor joins via video chat and can use the connected medical devices allowing a doctor to provide a thorough, safe, clinical examination. This means we treat more conditions than any other service. Any required medication is immediately available at the local pharmacy (most Medicspot clinics are within pharmacies).

medicspot = [...team, you];
---------------------------

We're a small, fast moving and growing team. We're friendly, supportive and ambitious and have a big vision to improve lives through healthcare innovation. We're have a strong culture of trust and respect and are looking for an engineer who has the aptitude to learn and apply themselves to build amazing products. We love delivering products which our patients and customers love, solving problems at their core and are continually looking for better ways to work. 

import x from 'you';
--------------------

Medicspot software is built with developer friendliness and productivity in mind. So expect `docker-compose up` to get everything up and running. We like products to be beautiful inside and out. We create clean, consistent react and node architectures for webapps and API services.

You should be experienced and flexible when designing and creating backend services, single page apps and databases. 

We're a JavaScript house front and back and even use ChromeOS on our stations. Good experience with SOA, JS, testing, docker, HTML5, accessible UX and all desirable.

The Medicspot system is made up of many individual products which work together to power the service our patients love. We're looking for a JS developer who can gel as part of the wider team and offer support in a variety of capacities. You'll be a key person in helping make the development team the very best they can be.

We're a high growth company, doubling in size every 4 months. This provides a lot of career growth opportunities as our team and product evolves.

Hopefully you like:

* Chipping in with different disciplines and helping the wider team
* Well defined and simple module/app concerns and interfaces
* JS (node 10+, ES6+)
* React (or at least the declarative pattern it embodies)
* Flat service oriented architectures (REST, JWT)
* Postgres
* Sensible testing
* VCS, CI, CD

We're learning all the time so hope you will share tools and practices to make this even better!

@returns {Promise} fun, challenge, cash and perks
-------------------------------------------------

As one of a small team of (currently six) engineers you are a super important hire who we'll help grow to lead some of our products. I hope the team, vision and product will be enough to convince you of the opportunity but please tell us what makes you tick and we'll do our best to accommodate.

For the starter package we're thinking:

* Flexible working hours/locations
* Work from home or the office
* 25 days + bank holidays
* Equipment (MPB or equivalent) & training budget

I've been in startups where everyone's been so absorbed by the challenge of the work that good people who have learned new valuable skills have been neglected. We'll regularly review the package to ensure great team players are well rewarded.

Now, I can't promise the shiny offices of big companies yet, in fact we're all working from home with Covid. But with the pace of change I'll guarantee that in a year you'll look back fondly as part of a team you're proud of. We plan to have a product team of 10-20 and shiny offices too but this time you'll have had a hand in choosing it!

So please apply if you think you'll be a good fit and want to be a part of our journey.

The interview process is short but sweet; 
1. Send us a few words as to why you want to get involved with a CV. 
2. We'll then have chat aligning on what we do, the role and what makes us tick. 
3. We have a fun little code challenge which we will go through in an extended meeting with some of the rest of the team. 
4. In the meeting we talk tech and see where the conversation leads us. At the end you can have a discussion with the team about working at Medicspot. 

As a bonus we also like everyone to have a quick chat with another director before starting to get a feel for the wider team. If you have any questions don't be afraid to ask.

All the best in whatever path you choose and hope to meet you soon!

Oli


If you're an agent only get in touch if you really know your stuff. In contacting Medicspot you are agreeing to the recruiter [terms and conditions](../contracts/recruitment_agents.md).