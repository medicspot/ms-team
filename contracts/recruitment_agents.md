Recruitment agents
==================

Introduction
------------

Medicspot welcome introductions to great people from recruitment agents. By establishing one contract for all agents it keeps things fair, simple and efficient. The contract below is designed to:

* ensure applicants receive their full offer package
* to let agents operate freely in finding and introducing great talent
* to let Medicspot operate efficiently to meet and onboard new team members.

For example, with this contract in place you, as the agent, can forward relevant CVs (unredacted with full contact details) knowing you'll get appropriate remuneration if the candidate is successful. We've found it much less effort overall for all parties.

Please read the contract below carefully and confirm your acceptance when communicating with Medicspot. Any unsolicited communications without acceptance of the contract is sadly likely to be ignored as these instructions are clear and fair.

To accept this contract, copy-and-paste the contract text below into your initial communication with Medicspot and add your name, company name, registered address and registered company number.

The Contract
------------

I, the Agent [the Agent], in approaching Medicspot Limited, it's related companies or employees [Medicspot] by any means including but not limited to email, phone calls and mail [Communications] regarding a position of employment [Position] for individuals or teams [Candidate] am in doing so accepting the following terms of engagement [Terms] which preceed and superceed any other form of agreement between the Agent and Medicspot.

* The only chargeable service between the Agent an Medicspot is introductions to successful Candidate(s).
* A maximum fee of 5% of the offered salary for first year of the Position is the only remuneration the Agent may request from Medicspot [Agent Fee]. All other forms of remeration the Candidate may benefit from including but not limited to equity, options, pensions or perks are excluded from the Agent Fee.
* A probation period of 3 months will apply to any Candidate [Probation Period]. If the Candidate or Medicspot decide for any reason that the Candidate is not suitable for the Position no Agent Fee(s) will be due.
* Medicspot agree to pay the Agent Fee promptly within 14 days of the end of the probation period.
* When a Candidate is put forward by The Agent for a Position, full contact details must be provided.
* Once a Candidate has accepted an offer from Medicspot the Agent must not send any Communications to the Candidate other than to finalise the Position. The Agent must do this within 14 days of the end of the Probation Period. The Agent agrees that if this condition is breached the Agent must pay a minimum fee of 10% of the offer salary is  from the Agent to Medicspot immediately.
* The Agent must not intentionally contact any employee or person(s) associated with Medicspot about roles outside of Medicspot. The Agent agrees that if this condition is breached a fee of 5% of the offer salary will be payable to Medicspot immediately.
* The Agent agrees not to surruptitiously introduce any new contract such as attaching or linking to contracts to emails. The Agent agrees that if this condition is breached a fee of 5% of the offer salary will be payable to Medicspot immediately.
* A new contract may be entered into for further services should a track record of quality hires be established. This will be discussed openly and plainly.


I [name] am fully authorized to and hereby accept the terms in this contract on behalf of [company name] of [registered address] with registration number [company number].